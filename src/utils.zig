const std = @import("std");

pub const FontColor = enum(u8) {
    Black = 30,
    Red = 31,
    Green = 32,
    Yellow = 33,
    Blue = 34,
    Magenta = 35,
    Cyan = 36,
    White = 37,
    Default = 39,
};

pub const BackgroundColor = enum(u8) {
    Black = 40,
    Red = 41,
    Green = 42,
    Yellow = 43,
    Blue = 44,
    Magenta = 45,
    Cyan = 46,
    White = 47,
    Default = 49,
};

pub const Bold: u8 = 1;
pub const UnsetBold: u8 = 22;

pub fn print_colored(comptime fmt: []const u8, args: anytype, foreground: FontColor, background: BackgroundColor, is_bold: bool) void {
    var final_output: [(fmt.len + 10) * 10]u8 = undefined;
    const final_output_slice = std.fmt.bufPrint(&final_output, fmt, args) catch unreachable;

    std.debug.print("\u{001b}[{d};{d};{d}m{s}\u{001b}[0m", .{ @intFromEnum(foreground), @intFromEnum(background), if (is_bold) Bold else UnsetBold, final_output_slice });
}

pub fn Reader(comptime T: type) type {
    return struct {
        buffer: []const T,
        read: usize,

        const Self = @This();

        pub fn init(buffer: []const T) Self {
            return .{ .buffer = buffer, .read = 0 };
        }

        pub fn check_not_eof(self: *const Self, offset: usize) ?void {
            if (self.read + offset == self.buffer.len) {
                return null;
            }
        }

        pub fn peek(self: *const Self) ?T {
            return self.n_peek(0);
        }

        pub fn n_peek(self: *const Self, offset: usize) ?T {
            self.check_not_eof(offset) orelse return null;

            return self.buffer[self.read + offset];
        }

        pub fn next(self: *Self) ?T {
            return self.nth(0);
        }

        ///Consumes until the offset is reached
        pub fn nth(self: *Self, offset: usize) ?T {
            const c = self.n_peek(offset) orelse return null;

            self.read += offset + 1;
            return c;
        }

        pub fn consume_spaces(self: *Self) void {
            while (true) {
                const c = self.peek() orelse break;
                if (c != ' ' and c != '\n' and c != '\r' and c != '\t') {
                    return;
                }
                self.read += 1;
            }
        }

        pub fn consume_until_char(self: *Self, delimiter: T) void {
            while (self.peek()) |c| {
                if (c == delimiter) return;
                self.read += 1;
            }
        }

        pub fn peek_slice(self: *const Self, size: usize) ?[]const T {
            if (self.read + size > self.buffer.len) return null;
            return self.buffer[self.read .. self.read + size];
        }
    };
}

pub fn Writer(comptime T: type) type {
    return struct {
        buffer: []T,
        written: usize,

        const Self = @This();

        pub fn init(buffer: []T) Self {
            return .{ .buffer = buffer, .written = 0 };
        }

        pub fn check_not_eof(self: *const Self, offset: usize) !void {
            if (self.written + offset == self.buffer.len) {
                return error.EOF;
            }
        }

        pub fn write_one(self: *Self, element: T) !void {
            try self.check_not_eof(0);

            self.buffer[self.written] = element;
            self.written += 1;
        }

        pub fn write_slice(self: *Self, s: []const T) !void {
            try self.check_not_eof(s.len - 1);

            @memcpy(self.buffer[self.written .. self.written + s.len], s);
            self.written += s.len;
        }
    };
}
