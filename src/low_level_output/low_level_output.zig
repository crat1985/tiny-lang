const Number = @import("../tokenizer/number.zig").Number;
const std = @import("std");
const FnDeclaration = @import("../parser/fn_declaration.zig").FnDeclaration;
const Operator = @import("../parser/cmp.zig").Operator;
const LowLevelValue = @import("low_level_value.zig").LowLevelValue;
const fn_decl_import = @import("fn_declaration.zig");

pub const SignedNumber = struct {
    nb: Number,
    is_positive: bool,
};

//Relative to the base ptr
pub const StackAllocation = struct {
    start: SignedNumber,
    size: Number,
};

pub const Command = union(enum) {
    Section: []const u8,
    Global: []const u8,
    Label: []const u8,
    JmpToLabel: []const u8,
    StackFrameStart: void,
    StackFrameEnd: void,
    /// Reservation of x bytes on the stack (fn call args, variable declaration)
    StackReservation: Number,
    StackAssignation: struct {
        alloc: StackAllocation,
        value: LowLevelValue,
        ///If we should use `movz` instead of `mov`
        is_zeroed: bool,
    },
    /// Cleanup of x bytes
    StackCleanup: Number,
    Ret: void,
    Call: []const u8,
    ///The name of the functon
    StdFnDeclaration: []const u8,
    Cmp: struct {
        base_value: LowLevelValue,
        compare_value: LowLevelValue,
    },
    ConditionalJump: struct {
        t: Operator,
        label: []const u8,
    },
};

pub const LowLevelOutput = struct {
    commands: []Command,

    var cmp_id: usize = 0;
    var block_id: usize = 0;
    var std_fns: std.StringHashMap(void) = undefined;
    var commands: std.ArrayList(Command) = undefined;
    var allocator: std.mem.Allocator = undefined;

    const Self = @This();

    pub fn generate(syntax_tree: *std.StringHashMap(FnDeclaration), alloc: std.mem.Allocator) !LowLevelOutput {
        //TODO add the .data and .bss sections

        allocator = alloc;

        commands = std.ArrayList(Command).init(allocator);

        try commands.append(Command{ .Section = ".text" });
        try commands.append(Command{ .Global = "_start" });

        var start_fn = syntax_tree.fetchRemove("_start").?.value;

        std_fns = std.StringHashMap(void).init(allocator);
        defer std_fns.deinit();

        try fn_decl_import.generate_fn_decl(&start_fn, allocator, &commands, &std_fns);

        var iterator = syntax_tree.valueIterator();

        while (iterator.next()) |fn_decl| {
            try fn_decl_import.generate_fn_decl(fn_decl, allocator, &commands, &std_fns);
        }

        var std_fns_keys_iterator = std_fns.keyIterator();

        while (std_fns_keys_iterator.next()) |std_fn_name| {
            try commands.append(Command{ .StdFnDeclaration = std_fn_name.* });
        }

        return .{
            .commands = try commands.toOwnedSlice(),
        };
    }
};
