const FnCall = @import("../parser/fn_call.zig").FnCall;
const Number = @import("../tokenizer/number.zig").Number;
const std = @import("std");
const StackAllocation = @import("low_level_output.zig").StackAllocation;
const Command = @import("low_level_output.zig").Command;
const low_level_value = @import("low_level_value.zig");

pub fn generate_fn_call(
    fn_call: FnCall,
    var_offset: *Number,
    var_indexes: *std.StringHashMap(StackAllocation),
    commands: *std.ArrayList(Command),
    allocator: std.mem.Allocator,
    std_fns: *std.StringHashMap(void),
) !void {
    try commands.append(Command{ .StackReservation = fn_call.ret_size });
    var_offset.* = var_offset.add(fn_call.ret_size);

    var args_total_size = Number{};

    for (0..fn_call.args.len) |i| {
        const fn_arg = fn_call.args[fn_call.args.len - i - 1]; //get the arg in inverse order
        try commands.append(Command{ .StackReservation = fn_arg.size });
        try commands.append(Command{
            .StackAssignation = .{
                .alloc = .{
                    .start = .{
                        .nb = var_offset.add(fn_arg.size).add(args_total_size),
                        .is_positive = false,
                    },
                    .size = fn_arg.size,
                },
                .value = try low_level_value.value_to_low_level_value(fn_arg.value, var_offset, var_indexes, commands, allocator, std_fns),
                .is_zeroed = true, //TODO not sure about that
            },
        });
        args_total_size = args_total_size.add(fn_arg.size);
    }

    const fn_name: []const u8 = if (fn_call.is_std) blk: {
        try std_fns.put(fn_call.fn_name, {});
        break :blk fn_call.fn_name;
    } else if (std.mem.eql(u8, fn_call.fn_name, "_start")) "_start" else blk: {
        const old_name = fn_call.fn_name;
        const prefix = "user_";
        const new_name = try allocator.alloc(u8, prefix.len + old_name.len);
        @memcpy(new_name[0..prefix.len], prefix);
        @memcpy(new_name[prefix.len..], old_name);
        break :blk new_name;
    };

    try commands.append(Command{ .Call = fn_name });

    try commands.append(Command{ .StackCleanup = args_total_size });
}
