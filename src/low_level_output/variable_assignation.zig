const VariableAssignation = @import("../parser/variable_assignation.zig").VariableAssignation;
const std = @import("std");
const StackAllocation = @import("low_level_output.zig").StackAllocation;
const Number = @import("../tokenizer/number.zig").Number;
const low_level_value_import = @import("low_level_value.zig");
const Command = @import("low_level_output.zig").Command;

pub fn generate_variable_assignation(
    var_assign: VariableAssignation,
    var_indexes: *std.StringHashMap(StackAllocation),
    var_offset: *Number,
    commands: *std.ArrayList(Command),
    allocator: std.mem.Allocator,
    std_fns: *std.StringHashMap(void),
) !void {
    const alloc = var_indexes.get(var_assign.var_name) orelse std.debug.panic("Cannot assign to undeclared variable {s}", .{var_assign.var_name});

    const low_level_value = try low_level_value_import.value_to_low_level_value(var_assign.value, var_offset, var_indexes, commands, allocator, std_fns);

    try commands.append(Command{ .StackAssignation = .{ .alloc = alloc, .value = low_level_value, .is_zeroed = var_assign.is_zeroed } });
}
