const VariableDeclaration = @import("../parser/variable_declaration.zig").VariableDeclaration;
const Number = @import("../tokenizer/number.zig").Number;
const std = @import("std");
const StackAllocation = @import("low_level_output.zig").StackAllocation;
const Command = @import("low_level_output.zig").Command;

pub fn generate_var_decl(
    var_decl: VariableDeclaration,
    var_offset: *Number,
    var_indexes: *std.StringHashMap(StackAllocation),
    commands: *std.ArrayList(Command),
) !void {
    try var_indexes.put(var_decl.name, StackAllocation{
        .start = .{ .nb = var_offset.add(var_decl.size), .is_positive = false },
        .size = var_decl.size,
    });

    var_offset.* = var_offset.add(var_decl.size);

    try commands.append(Command{ .StackReservation = var_decl.size });
}
