const FnDeclaration = @import("../parser/fn_declaration.zig").FnDeclaration;
const FnReturn = @import("../parser/fn_return.zig").FnReturn;
const StackAllocation = @import("low_level_output.zig").StackAllocation;
const Number = @import("../tokenizer/number.zig").Number;
const std = @import("std");
const Command = @import("low_level_output.zig").Command;
const low_level_value_import = @import("low_level_value.zig");

pub fn generate_fn_return(
    fn_decl: FnDeclaration,
    fn_ret: FnReturn,
    fn_ret_place: StackAllocation,
    ret_label: []const u8,
    var_offset: *Number,
    var_indexes: *std.StringHashMap(StackAllocation),
    commands: *std.ArrayList(Command),
    allocator: std.mem.Allocator,
    std_fns: *std.StringHashMap(void),
) !void {
    if (!fn_decl.ret_size.is_null()) {
        try commands.append(Command{
            .StackAssignation = .{
                .alloc = .{
                    .start = fn_ret_place.start,
                    .size = fn_ret_place.size,
                },
                .value = try low_level_value_import.value_to_low_level_value(fn_ret.value.?, var_offset, var_indexes, commands, allocator, std_fns),
                .is_zeroed = true, //TODO not sure
            },
        });
    }

    try commands.append(Command{ .JmpToLabel = ret_label });
}
