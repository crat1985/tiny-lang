const low_level_value_import = @import("low_level_value.zig");
const Cmp = @import("../parser/cmp.zig").Cmp;
const std = @import("std");
const Command = @import("low_level_output.zig").Command;
const StackAllocation = @import("low_level_output.zig").StackAllocation;
const fn_child_import = @import("fn_child.zig");
const FnDeclaration = @import("../parser/fn_declaration.zig").FnDeclaration;
const Number = @import("../tokenizer/number.zig").Number;
const Block = @import("block.zig").Block;

var cmp_count: usize = 0;

pub fn generate_cmp(
    cmp: Cmp,
    commands: *std.ArrayList(Command),
    var_offset: *Number,
    allocator: std.mem.Allocator,
    std_fns: *std.StringHashMap(void),
    var_indexes: *std.StringHashMap(StackAllocation),
    fn_decl: FnDeclaration,
    return_space: StackAllocation,
    ret_label: []const u8,
    blocks_above: *std.ArrayList(Block),
) anyerror!void {
    const base_value = try low_level_value_import.value_to_low_level_value(cmp.base_value, var_offset, var_indexes, commands, allocator, std_fns);
    const compare_value = try low_level_value_import.value_to_low_level_value(cmp.compare_value, var_offset, var_indexes, commands, allocator, std_fns);

    try commands.append(Command{ .Cmp = .{ .base_value = base_value, .compare_value = compare_value } });

    var cmp_labels_and_children = std.ArrayList(Command).init(allocator);

    const cmp_end_label = try std.fmt.allocPrint(allocator, "cmpend_{d}", .{cmp_count});

    {
        var cmp_conditions_i: usize = 0;

        for (cmp.children.items) |entry| {
            const cmp_label = try std.fmt.allocPrint(allocator, "cmp_{d}_{d}", .{ cmp_count, cmp_conditions_i });

            for (entry.@"0") |operator| {
                try commands.append(Command{ .ConditionalJump = .{
                    .t = operator,
                    .label = cmp_label,
                } });
            }

            try cmp_labels_and_children.append(Command{ .Label = cmp_label });

            for (entry.@"1") |child| {
                try fn_child_import.generate_fn_child(
                    child,
                    var_offset,
                    var_indexes,
                    fn_decl,
                    return_space,
                    ret_label,
                    blocks_above,
                    &cmp_labels_and_children,
                    allocator,
                    std_fns,
                );
            }

            cmp_conditions_i += 1;
        }
    }

    try commands.append(Command{ .JmpToLabel = cmp_end_label });

    for (cmp_labels_and_children.items) |command| {
        try commands.append(command);
    }

    try commands.append(Command{ .Label = cmp_end_label });
}
