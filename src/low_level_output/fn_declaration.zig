const FnDeclaration = @import("../parser/fn_declaration.zig").FnDeclaration;
const std = @import("std");
const Block = @import("block.zig").Block;
const Command = @import("low_level_output.zig").Command;
const StackAllocation = @import("low_level_output.zig").StackAllocation;
const Number = @import("../tokenizer/number.zig").Number;
const fn_child_import = @import("fn_child.zig");

pub fn generate_fn_decl(
    fn_decl: *FnDeclaration,
    allocator: std.mem.Allocator,
    commands: *std.ArrayList(Command),
    std_fns: *std.StringHashMap(void),
) !void {
    var blocks_above = std.ArrayList(Block).init(allocator);

    //generate the fn name with the "user_" prefix (expect for _start)
    {
        const fn_name = if (std.mem.eql(u8, fn_decl.name, "_start")) "_start" else blk: {
            const prefix = "user_";
            const fn_name = try allocator.alloc(u8, fn_decl.name.len + prefix.len);
            @memcpy(fn_name[0..prefix.len], prefix);
            @memcpy(fn_name[prefix.len..], fn_decl.name);
            break :blk fn_name;
        };

        fn_decl.name = fn_name;
    }

    const ret_label = try std.fmt.allocPrint(allocator, "ret_{s}", .{fn_decl.name});

    try commands.append(Command{ .Label = fn_decl.name });

    try commands.append(Command{ .StackFrameStart = {} });

    var var_indexes = std.StringHashMap(StackAllocation).init(allocator);

    //Skip the ret addr and the push of the base ptr on the stack in the stack frame beginning
    var args_offset = Number{ .ref_count = 3 };

    for (fn_decl.args) |fn_arg| {
        try var_indexes.put(
            fn_arg.name,
            .{
                .start = .{
                    .nb = args_offset,
                    .is_positive = true,
                },
                .size = fn_arg.size,
            },
        );
        args_offset = args_offset.add(fn_arg.size);
    }

    const return_space: StackAllocation = .{
        .start = .{
            .nb = args_offset,
            .is_positive = true,
        },
        .size = fn_decl.ret_size,
    };

    //Negative, relative to the base ptr
    var var_offset = Number{};

    for (fn_decl.body) |fn_child| {
        try fn_child_import.generate_fn_child(
            fn_child,
            &var_offset,
            &var_indexes,
            fn_decl.*,
            return_space,
            ret_label,
            &blocks_above,
            commands,
            allocator,
            std_fns,
        );
    }

    try commands.append(Command{ .Label = ret_label });
    try commands.append(Command{ .StackFrameEnd = {} });
    try commands.append(Command{ .Ret = {} });
}
