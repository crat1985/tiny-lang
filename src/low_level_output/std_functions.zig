const Number = @import("../tokenizer/number.zig").Number;
const FnArg = @import("../parser/fn_declaration.zig").FnArg;
const FnDeclaration = @import("../parser/fn_declaration.zig").FnDeclaration;
const std = @import("std");

pub fn get_std_functions(allocator: std.mem.Allocator) !std.StringHashMap(FnDeclaration) {
    var hashmap = std.StringHashMap(FnDeclaration).init(allocator);

    //exit fn
    {
        const args = try allocator.alloc(FnArg, 1);
        args[0] = FnArg{
            .name = "status_code",
            .size = .{ .ref_count = 1 },
            .start = 0,
            .end = 0,
        };

        try hashmap.put("exit", .{ .name = "exit", .args = args, .body = &.{}, .ret_size = .{}, .start = 0, .end = 0 });
    }

    return hashmap;
}
