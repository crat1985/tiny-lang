const StackAllocation = @import("low_level_output.zig").StackAllocation;
const Number = @import("../tokenizer/number.zig").Number;
const Value = @import("../parser/value.zig").Value;
const std = @import("std");
const fn_call_import = @import("fn_call.zig");
const Command = @import("low_level_output.zig").Command;

pub const LowLevelValue = union(enum) {
    alloc: StackAllocation,
    literal: Number,
};

pub fn value_to_low_level_value(
    value: Value,
    var_offset: *Number,
    var_indexes: *std.StringHashMap(StackAllocation),
    commands: *std.ArrayList(Command),
    allocator: std.mem.Allocator,
    std_fns: *std.StringHashMap(void),
) anyerror!LowLevelValue {
    return switch (value) {
        .fn_call => |fn_call| blk: {
            try fn_call_import.generate_fn_call(fn_call, commands, var_offset, allocator, std_fns, var_indexes);

            const alloc = StackAllocation{ .start = .{ .nb = var_offset.*, .is_positive = false }, .size = fn_call.ret_size };

            break :blk LowLevelValue{ .alloc = alloc };
        },
        .variable => |var_name| blk: {
            const alloc = var_indexes.get(var_name) orelse std.debug.panic("Undeclared variable {s}", .{var_name});

            break :blk LowLevelValue{ .alloc = alloc };
        },
        .literal => |nb| LowLevelValue{ .literal = nb },
    };
}
