const FnChild = @import("../parser/fn_child.zig").FnChild;
const Number = @import("../tokenizer/number.zig").Number;
const std = @import("std");
const StackAllocation = @import("low_level_output.zig").StackAllocation;
const FnDeclaration = @import("../parser/fn_declaration.zig").FnDeclaration;
const Block = @import("block.zig").Block;
const fn_call_import = @import("fn_call.zig");
const Command = @import("low_level_output.zig").Command;
const var_decl_import = @import("var_declaration.zig");
const var_assign_import = @import("variable_assignation.zig");
const fn_return_import = @import("fn_return.zig");
const block_import = @import("block.zig");
const low_level_value_import = @import("low_level_value.zig");
const cmp_import = @import("cmp.zig");

pub fn generate_fn_child(
    fn_child: FnChild,
    var_offset: *Number,
    var_indexes: *std.StringHashMap(StackAllocation),
    fn_decl: FnDeclaration,
    return_space: StackAllocation,
    ret_label: []const u8,
    blocks_above: *std.ArrayList(Block),
    commands: *std.ArrayList(Command),
    allocator: std.mem.Allocator,
    std_fns: *std.StringHashMap(void),
) !void {
    switch (fn_child) {
        .fn_call => |fn_call| try fn_call_import.generate_fn_call(fn_call, var_offset, var_indexes, commands, allocator, std_fns),
        .variable_declaration => |var_decl| try var_decl_import.generate_var_decl(var_decl, var_offset, var_indexes, commands),
        .variable_assignation => |var_assign| try var_assign_import.generate_variable_assignation(var_assign, var_indexes, var_offset, commands, allocator, std_fns),
        .ret => |fn_ret| try fn_return_import.generate_fn_return(
            fn_decl,
            fn_ret,
            return_space,
            ret_label,
            var_offset,
            var_indexes,
            commands,
            allocator,
            std_fns,
        ),
        .block => |block| try block_import.generate_block(
            block.children,
            var_offset,
            var_indexes,
            fn_decl,
            return_space,
            ret_label,
            blocks_above,
            block.is_loop,
            allocator,
            commands,
            std_fns,
        ),
        .@"break" => |break_struct| {
            const id = break_struct.break_id;
            const absolute_id = blocks_above.items[blocks_above.items.len - id - 1].index;

            const end_label = try std.fmt.allocPrint(allocator, "blockend_{d}", .{absolute_id});
            try commands.append(Command{ .JmpToLabel = end_label });
        },
        .@"continue" => |loop| {
            //TODO perhaps skip all the non-loop blocks (so 0 begin the first block being a loop, not the first block)
            const id = loop.continue_id;
            const block = blocks_above.items[blocks_above.items.len - id - 1];
            std.debug.assert(block.is_loop);

            const begin_label = try std.fmt.allocPrint(allocator, "blockcontinue_{d}", .{block.index});
            try commands.append(Command{ .JmpToLabel = begin_label });
        },
        .cmp => |cmp| try cmp_import.generate_cmp(cmp, commands, var_offset, allocator, std_fns, var_indexes, fn_decl, return_space, ret_label, blocks_above),
    }
}
