const FnChild = @import("../parser/fn_child.zig").FnChild;
const Number = @import("../tokenizer/number.zig").Number;
const std = @import("std");
const StackAllocation = @import("low_level_output.zig").StackAllocation;
const FnDeclaration = @import("../parser/fn_declaration.zig").FnDeclaration;
const Command = @import("low_level_output.zig").Command;
const fn_child_import = @import("fn_child.zig");

pub const Block = struct {
    index: usize,
    is_loop: bool,
};

var block_id: usize = 0;

pub fn generate_block(
    children: []FnChild,
    var_offset: *Number,
    var_indexes: *std.StringHashMap(StackAllocation),
    fn_decl: FnDeclaration,
    return_space: StackAllocation,
    ret_label: []const u8,
    blocks_above: *std.ArrayList(Block),
    is_loop: bool,
    allocator: std.mem.Allocator,
    commands: *std.ArrayList(Command),
    std_fns: *std.StringHashMap(void),
) anyerror!void {
    try blocks_above.append(.{ .index = block_id, .is_loop = is_loop });

    const block_label: []u8 = if (is_loop) blk: {
        const label = try std.fmt.allocPrint(allocator, "block_{d}", .{block_id});
        try commands.append(Command{ .Label = label });
        break :blk label;
    } else undefined;

    const blockend_label = try std.fmt.allocPrint(allocator, "blockend_{d}", .{block_id});
    const blockcontinue_label: []u8 = if (is_loop) try std.fmt.allocPrint(allocator, "blockcontinue_{d}", .{block_id}) else undefined;

    block_id += 1;

    const base_var_offset = var_offset.*;

    for (children) |child| {
        try fn_child_import.generate_fn_child(
            child,
            var_offset,
            var_indexes,
            fn_decl,
            return_space,
            ret_label,
            blocks_above,
            commands,
            allocator,
            std_fns,
        );
    }

    //clear all the variables after each iteration
    if (is_loop) {
        try commands.append(Command{ .Label = blockcontinue_label });

        try commands.append(Command{ .StackCleanup = var_offset.sub(base_var_offset) });

        try commands.append(Command{ .JmpToLabel = block_label });
    }

    try commands.append(Command{ .Label = blockend_label });

    try commands.append(Command{ .StackCleanup = var_offset.sub(base_var_offset) });

    var_offset.* = base_var_offset;

    _ = blocks_above.pop();
}
