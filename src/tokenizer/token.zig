const Ident = @import("ident.zig").Ident;
const Punct = @import("punct.zig").Punct;
const Number = @import("number.zig").Number;
const Reader = @import("../utils.zig").Reader;

pub const TokenType = union(enum) {
    ident: Ident,
    punct: Punct,
    number: Number,

    const Self = @This();

    pub fn parse(reader: *Reader(u8)) !Self {
        return Self{ .ident = try Ident.parse(reader) orelse return Self{ .punct = Punct.parse(reader) orelse return Self{ .number = try Number.parse(reader) orelse unreachable } } };
    }
};

pub const Token = struct { t: TokenType, start: usize, end: usize };
