const std = @import("std");
const Reader = @import("../utils.zig").Reader;

pub const Ident = union(enum) {
    Fn: void,
    Let: void,
    Cmp: void,
    Loop: void,
    Return: void,
    Break: void,
    Continue: void,
    Deref: void,
    Mov: void,
    Movz: void,
    Other: CustomIdent,

    pub fn parse(reader: *Reader(u8)) !?Ident {
        var begin_i = reader.read;

        var prefix: ?IdentPrefix = null;

        while (true) {
            const c = reader.peek().?;
            if (begin_i == reader.read) {
                if (c == '@') {
                    prefix = .At;
                    begin_i += 1;
                }

                if (!std.ascii.isAlphabetic(c) and c != '@' and c != '_')
                    return null; //Not an ident
            } else {
                if (!std.ascii.isAlphanumeric(c) and c != '_') break;
            }

            _ = reader.next().?;
        }

        const ident = reader.buffer[begin_i..reader.read];

        //Check if it is a reserved keyword
        inline for (std.meta.fields(Ident)) |field| {
            if (comptime std.mem.eql(u8, field.name, "Other")) continue;

            if (field.name.len == ident.len) {
                var lowercase_field_name: [field.name.len]u8 = undefined;
                for (field.name, 0..) |c, i| {
                    lowercase_field_name[i] = std.ascii.toLower(c);
                }
                if (std.mem.eql(u8, ident, &lowercase_field_name)) {
                    std.debug.assert(prefix == null);
                    return @unionInit(Ident, field.name, {});
                }
            }
        }

        return .{ .Other = .{ .value = ident, .prefix = prefix } };
    }
};

pub const IdentPrefix = enum {
    At,
};

pub const CustomIdent = struct {
    value: []const u8,
    prefix: ?IdentPrefix,
};
