const Reader = @import("../utils.zig").Reader;
const std = @import("std");

pub const Number = struct {
    literal: u64 = 0,
    ref_count: usize = 0,

    const Self = @This();

    pub fn add(self: Self, other: Self) Self {
        return Self{
            .literal = self.literal + other.literal,
            .ref_count = self.ref_count + other.ref_count,
        };
    }

    pub fn sub(self: Self, other: Self) Self {
        return Self{
            .literal = self.literal - other.literal,
            .ref_count = self.ref_count - other.ref_count,
        };
    }

    pub fn is_null(self: Self) bool {
        return self.literal == 0 and self.ref_count == 0;
    }

    pub fn get(self: Self, ptr_size: u8) usize {
        return self.literal + self.ref_count * ptr_size;
    }

    pub fn parse(reader: *Reader(u8)) !?Self {
        const first_c = reader.peek().?;

        if (std.ascii.isDigit(first_c)) {
            const beginning_index = reader.read;
            _ = reader.next().?;

            const perhaps_prefix = reader.peek() orelse
                return .{ .literal = first_c - '0' }; //return if the number is finished

            if (!std.ascii.isDigit(perhaps_prefix) and perhaps_prefix != 'b' and perhaps_prefix != 'o' and perhaps_prefix != 'x') {
                return .{ .literal = first_c - '0' }; //return if the number is finished
            }

            _ = reader.next().?;

            while (true) {
                const local_c = reader.peek() orelse break;
                if (!std.ascii.isDigit(local_c)) {
                    break;
                }
                _ = reader.next().?;
            }

            const nb = try std.fmt.parseInt(usize, reader.buffer[beginning_index..reader.read], 0);
            return Number{ .literal = nb };
        }

        //If it's not a number literal, then it should be the `ref` keyword
        if (first_c != 'r') return null;

        if (!std.mem.eql(u8, reader.peek_slice(3) orelse return null, "ref")) return null;

        _ = reader.nth(2).?;

        return .{ .ref_count = 1 };
    }
};
