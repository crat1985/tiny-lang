const std = @import("std");
const Reader = @import("../utils.zig").Reader;
pub const Token = @import("token.zig").Token;
const TokenType = @import("token.zig").TokenType;
const Ident = @import("ident.zig").Ident;
const Punct = @import("punct.zig").Punct;
const Number = @import("number.zig").Number;

pub const Tokenizer = struct {
    reader: Reader(u8),

    const Self = @This();

    pub fn init(content: []const u8) Self {
        return .{ .reader = Reader(u8).init(content) };
    }

    pub fn tokenize(self: *Self, allocator: std.mem.Allocator) ![]Token {
        var array = std.ArrayList(Token).init(allocator);

        while (true) {
            self.reader.consume_spaces();

            const c = self.reader.peek() orelse break; //break if EOF

            //Skip comments
            blk: {
                if (c != '/') {
                    break :blk;
                }

                const second_char = self.reader.n_peek(1) orelse break :blk;
                if (second_char != '/') {
                    break :blk;
                }

                _ = self.reader.nth(1).?; //skip the 2 slashes (optional)

                self.reader.consume_until_char('\n');

                continue;
            }

            const start = self.reader.read;
            const token_type = try TokenType.parse(&self.reader);
            try array.append(.{ .start = start, .end = self.reader.read, .t = token_type });
        }

        return try array.toOwnedSlice();
    }
};
