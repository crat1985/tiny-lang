const Reader = @import("../utils.zig").Reader;

pub const Punct = enum(u8) {
    OpenParenthesis = '(',
    ClosingParenthesis = ')',
    Comma = ',',
    SemiColon = ';',
    Colon = ':',
    OpenBrace = '{',
    ClosingBrace = '}',
    Ampersand = '&',
    Plus = '+',

    pub fn parse(reader: *Reader(u8)) ?Punct {
        const c = switch (reader.peek() orelse return null) {
            '(' => Punct.OpenParenthesis,
            ')' => Punct.ClosingParenthesis,
            ',' => Punct.Comma,
            ';' => Punct.SemiColon,
            ':' => Punct.Colon,
            '{' => Punct.OpenBrace,
            '}' => Punct.ClosingBrace,
            '&' => Punct.Ampersand,
            '+' => Punct.Plus,
            else => return null,
        };

        _ = reader.next().?;

        return c;
    }
};
