const std = @import("std");
const Tokenizer = @import("tokenizer/tokenizer.zig").Tokenizer;
const Parser = @import("parser/parser.zig").Parser;
const Command = @import("cmd.zig").Command;
const utils = @import("utils.zig");
const LowLevelOutput = @import("low_level_output/low_level_output.zig").LowLevelOutput;
const get_std_functions = @import("low_level_output/std_functions.zig").get_std_functions;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    const cmd = try Command.parse(allocator);
    defer allocator.free(cmd.content);

    std.debug.print("Command : {any}\n", .{cmd});

    // try std.json.stringify(cmd, .{ .whitespace = .indent_2 }, std.io.getStdOut().writer());

    std.debug.print("\n", .{});

    var tokenizer = Tokenizer.init(cmd.content);
    const tokens = try tokenizer.tokenize(allocator);
    defer allocator.free(tokens);

    std.debug.print("Tokens :\n", .{});

    try std.json.stringify(tokens, .{ .whitespace = .indent_2 }, std.io.getStdOut().writer());

    std.debug.print("\n", .{});
}

fn todo() void {
    const allocator = {};
    const tokens = {};
    const cmd = {};
    const std_functions = try get_std_functions(allocator);

    var parser = try Parser.parse(tokens, allocator);
    try parser.set_fn_call_args_size_and_ret_size(std_functions);

    var fn_decl_iterator = parser.fn_decls.valueIterator();

    std.debug.print("Syntax tree :\n", .{});

    while (fn_decl_iterator.next()) |fn_decl| {
        //TODO fix that
        // try std.json.stringify(fn_decl.*, .{ .whitespace = .indent_2 }, std.io.getStdOut().writer());
        std.debug.print("{any}", .{fn_decl});
        std.debug.print("\n", .{});
    }

    std.debug.print("\n", .{});

    const low_level_output = try LowLevelOutput.generate(&parser.fn_decls, allocator);
    std.debug.print("Low level output :\n", .{});

    try std.json.stringify(low_level_output.commands, .{
        .whitespace = .indent_2,
    }, std.io.getStdOut().writer());

    std.debug.print("\n", .{});

    // syntax_tree.print_code();

    const output = try cmd.generator.generate(low_level_output.commands, allocator);
    defer allocator.free(output);

    const ext = ".asm";
    const asm_file_name = try allocator.alloc(u8, cmd.output_file.len + ext.len);
    @memcpy(asm_file_name[0 .. asm_file_name.len - ext.len], cmd.output_file);
    @memcpy(asm_file_name[asm_file_name.len - ext.len ..], ext);

    const output_asm_file = try std.fs.cwd().createFile(asm_file_name, .{});
    _ = try output_asm_file.write(output);
}
