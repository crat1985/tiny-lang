const std = @import("std");
const Generator = @import("code_generator/generator.zig").Generator;
const LinuxX86_64Generator = @import("code_generator/linux_x86_64.zig").LinuxX86_64Generator;

pub const Target = enum {
    Linux_x86,
    Linux_x86_64,
    Linux_arm,
    Linux_aarch64,

    const Self = @This();

    pub fn parse(input: []const u8) ?Target {
        if (std.mem.eql(u8, input, "linux-x86_64")) {
            return .Linux_x86_64;
        }

        if (std.mem.eql(u8, input, "linux-x86")) {
            return .Linux_x86;
        }

        if (std.mem.eql(u8, input, "linux-arm")) {
            return .Linux_arm;
        }

        if (std.mem.eql(u8, input, "linux-aarch64")) {
            return .Linux_aarch64;
        }

        return null;
    }

    pub fn get_ptr_size(self: *const Self) u8 {
        return switch (self.*) {
            .Linux_x86, .Linux_arm => @as(u8, 4),
            .Linux_x86_64, .Linux_aarch64 => @as(u8, 8),
            // else => @panic("idk"),
        };
    }
};

pub const Command = struct {
    content: []u8,
    output_file: []const u8,
    target: Target,
    generator: Generator,

    const Self = @This();

    pub fn parse(allocator: std.mem.Allocator) !Self {
        var iterator = std.process.args();
        _ = iterator.next().?; //skip the program path

        var source: ?[]const u8 = null;
        var output: ?[]const u8 = null;
        var target: ?Target = null;

        while (iterator.next()) |arg| {
            switch (arg[0]) {
                '-' => {
                    if (std.mem.eql(u8, arg, "-o")) {
                        std.debug.assert(output == null);
                        output = iterator.next().?;
                        continue;
                    }

                    if (std.mem.eql(u8, arg, "--target")) {
                        std.debug.assert(target == null);
                        target = Target.parse(iterator.next().?).?;
                        continue;
                    }

                    std.debug.panic("Unknown option `{s}`", .{arg});
                },
                else => {
                    if (output != null) {
                        @panic("Source file already set");
                    }
                    source = arg;
                },
            }
        }

        const dir = std.fs.cwd();

        const file = try dir.openFile(source.?, .{ .mode = .read_only });

        const content = try file.readToEndAlloc(allocator, 1_000_000);

        const target_with_default = target orelse Target.Linux_x86_64; //TODO detect the OS and arch

        const generator = switch (target_with_default) {
            .Linux_x86_64 => LinuxX86_64Generator.generator(),
            else => @panic("Unsupported target"),
        };

        return .{
            .content = content,
            .output_file = output orelse "output",
            .target = target_with_default,
            .generator = generator,
        };
    }
};
