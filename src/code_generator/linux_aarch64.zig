const Generator = @import("generator.zig").Generator;
const std = @import("std");
const Writer = @import("../utils.zig").Writer;
const FnChild = @import("../parser/fn_child.zig").FnChild;
const Command = @import("../low_level_output/low_level_output.zig").Command;
const Number = @import("../tokenizer/number.zig").Number;
const StackAllocation = @import("../low_level_output/low_level_output.zig").StackAllocation;

pub const LinuxAarch64Generator = struct {
    const STACK_SIZE: usize = 8;

    const Self = @This();

    pub fn generate_section(section: []const u8, writer: *Writer(u8)) !void {
        try writer.write_slice("section ");
        try writer.write_slice(section);
        try writer.write_one('\n');
    }

    pub fn generate_global(fn_name: []const u8, writer: *Writer(u8)) !void {
        try writer.write_slice("global ");
        try writer.write_slice(fn_name);
        try writer.write_one('\n');
    }

    pub fn generate_label(label: []const u8, writer: *Writer(u8)) !void {
        try writer.write_slice(label);
        try writer.write_slice(":\n");
    }

    pub fn generate_jump_to_label(label: []const u8, writer: *Writer(u8)) !void {
        try writer.write_slice("jmp ");

        try writer.write_slice(label);
        try writer.write_one('\n');
    }

    pub fn generate_stack_frame_start(writer: *Writer(u8)) !void {
        try writer.write_slice(
            \\;stack frame start
            \\push rbp
            \\mov rbp, rsp
            \\
        );
    }

    pub fn generate_stack_frame_end(writer: *Writer(u8)) !void {
        try writer.write_slice(
            \\;stack frame end
            \\mov rsp, rbp
            \\pop rbp
            \\
        );
    }

    pub fn generate_stack_reservation(size: usize, writer: *Writer(u8), allocator: std.mem.Allocator) !void {
        const output = try std.fmt.allocPrint(allocator, "sub rsp, {d}\n", .{size});
        defer allocator.free(output);

        try writer.write_slice(output);
    }

    pub fn generate_stack_assignation(alloc: StackAllocation, value: Number, writer: *Writer(u8), allocator: std.mem.Allocator) !void {
        const real_value = value.get(STACK_SIZE);
        const alloc_start = alloc.start.nb.get(STACK_SIZE);
        const sign = if (alloc.start.is_positive) @as(u8, '+') else '-';

        //TODO handle that better, for e.g., set individually each 8 bytes
        const output = try std.fmt.allocPrint(allocator, "mov qword [rbp{c}{d}], {d}\n", .{ sign, alloc_start, real_value });
        defer allocator.free(output);

        try writer.write_slice(output);
    }

    pub fn generate_stack_cleanup(size: Number, writer: *Writer(u8), allocator: std.mem.Allocator) !void {
        const output = try std.fmt.allocPrint(allocator, "add rsp, {d}\n", .{size.get(STACK_SIZE)});
        defer allocator.free(output);

        try writer.write_slice(output);
    }

    pub fn generator() Generator {
        return Generator{ .generate_fn = generate };
    }
};
