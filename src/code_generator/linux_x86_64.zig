const Generator = @import("generator.zig").Generator;
const std = @import("std");
const Writer = @import("../utils.zig").Writer;
const FnChild = @import("../parser/fn_child.zig").FnChild;
const Command = @import("../low_level_output/low_level_output.zig").Command;
const Number = @import("../tokenizer/number.zig").Number;
const StackAllocation = @import("../low_level_output/low_level_output.zig").StackAllocation;
const LowLevelValue = @import("../low_level_output/low_level_value.zig").LowLevelValue;

pub const LinuxX86_64Generator = struct {
    const STACK_SIZE: usize = 8;

    const Self = @This();

    const StdFunctions = std.static_string_map.StaticStringMap([]const u8).initComptime(.{
        .{
            "exit",
            \\exit:
            \\mov rax, 60
            \\mov rdi, qword [rsp+16]
            \\syscall
            \\
        },
    });

    pub fn generate_section(section: []const u8, writer: *Writer(u8)) !void {
        try writer.write_slice("section ");
        try writer.write_slice(section);
        try writer.write_one('\n');
    }

    pub fn generate_global(fn_name: []const u8, writer: *Writer(u8)) !void {
        try writer.write_slice("global ");
        try writer.write_slice(fn_name);
        try writer.write_one('\n');
    }

    pub fn generate_label(label: []const u8, writer: *Writer(u8)) !void {
        try writer.write_slice(label);
        try writer.write_slice(":\n");
    }

    pub fn generate_jump_to_label(label: []const u8, writer: *Writer(u8)) !void {
        try writer.write_slice("jmp ");

        try writer.write_slice(label);
        try writer.write_one('\n');
    }

    pub fn generate_stack_frame_start(writer: *Writer(u8)) !void {
        try writer.write_slice(
            \\;stack frame start
            \\push rbp
            \\mov rbp, rsp
            \\
        );
    }

    pub fn generate_stack_frame_end(writer: *Writer(u8)) !void {
        try writer.write_slice(
            \\;stack frame end
            \\mov rsp, rbp
            \\pop rbp
            \\
        );
    }

    pub fn generate_stack_reservation(size: Number, writer: *Writer(u8), allocator: std.mem.Allocator) !void {
        const output = try std.fmt.allocPrint(allocator, "sub rsp, {d}\n", .{size.get(STACK_SIZE)});
        defer allocator.free(output);

        try writer.write_slice(output);
    }

    ///Get the string that represent the position of the specified byte
    fn get_place_byte(byte: usize, first_byte: isize, allocator: std.mem.Allocator) ![]const u8 {
        const relative_byte = first_byte + @as(isize, @intCast(byte));
        return try std.fmt.allocPrint(
            allocator,
            "[rbp{s}{d}]",
            .{
                if (relative_byte >= 0) "+" else "",
                relative_byte,
            },
        );
    }

    const Size = enum(u8) {
        Byte = 1,
        Word = 2,
        Dword = 4,
        Qword = 8,

        const SizeSelf = @This();

        pub fn to_string(self: SizeSelf) []const u8 {
            return switch (self) {
                .Byte => "byte",
                .Word => "word",
                .Dword => "dword",
                .Qword => "qword",
            };
        }
    };

    ///Get the string representation of the data, and the size specifier if some
    fn get_value_byte(byte: usize, size: Size, local_value: LowLevelValue, allocator: std.mem.Allocator, writer: *Writer(u8)) ![]const u8 {
        return switch (local_value) {
            .alloc => |value_alloc| blk: {
                //Get the first byte of the value
                var pos: isize = @intCast(value_alloc.start.nb.get(STACK_SIZE));
                if (!value_alloc.start.is_positive) pos *= -1;

                //Get the byte specified
                pos += @intCast(byte);

                const eax_moving = try std.fmt.allocPrint(
                    allocator,
                    "mov eax, {s} [rbp{s}{d}]\n",
                    .{ size.to_string(), if (value_alloc.start.is_positive) "+" else "", pos },
                );
                defer allocator.free(eax_moving);

                try writer.write_slice(eax_moving);

                break :blk "eax";
            },
            .literal => |nb| blk: {
                var nb_as_le: [8]u8 = undefined;
                std.mem.writeInt(usize, &nb_as_le, nb.get(STACK_SIZE), .little);
                std.debug.assert(byte == 0 or byte == 4);
                std.debug.assert(size == .Dword); //TODO For now only 64 bits

                var nb_bits: [32]u8 = undefined;

                //Because it will be interpreted as big-endian but stored as little-endian by the Assembler
                for (0..4) |i| {
                    _ = try std.fmt.bufPrint(nb_bits[i * 8 .. i * 8 + 8], "{b:0>8}", .{nb_as_le[byte + (3 - i)]});
                }

                break :blk try std.fmt.allocPrint(allocator, "0b{s}", .{nb_bits});
            },
        };
    }
    pub fn generate_stack_assignation(
        alloc: StackAllocation,
        value: LowLevelValue,
        writer: *Writer(u8),
        allocator: std.mem.Allocator,
        is_zeroed: bool,
    ) !void {
        const value_size = switch (value) {
            .alloc => |value_alloc| value_alloc.size.get(STACK_SIZE),
            .literal => STACK_SIZE,
        };

        {
            const place_size = alloc.size.get(STACK_SIZE);

            if (is_zeroed) {
                if (value_size > place_size) {
                    std.debug.panic("Expected smaller or equal size value, got value of {d} bytes for a {d} bytes space", .{ value_size, place_size });
                }
            } else {
                if (value_size != place_size) {
                    std.debug.panic("Expected equal size value, got value of {d} bytes for a {d} bytes space", .{ value_size, place_size });
                }
            }
        }

        //The output first byte
        const place_first_byte = blk: {
            var byte: isize = @intCast(alloc.start.nb.get(STACK_SIZE));
            if (!alloc.start.is_positive) byte *= -1;
            break :blk byte;
        };

        // const qword_count = value_size / 8;
        const dword_count = value_size / 4;
        const has_one_word = value_size & 2 == 2;
        const has_one_byte = value_size & 1 == 1;

        var i: usize = 0;

        while (i < dword_count * 4) : (i += 4) {
            const place_byte = try get_place_byte(i, place_first_byte, allocator);
            const value_byte = try get_value_byte(i, .Dword, value, allocator, writer);

            const line = try std.fmt.allocPrint(allocator, "mov dword {s}, {s}\n", .{ place_byte, value_byte });
            defer allocator.free(line);

            try writer.write_slice(line);
        }

        if (has_one_word) {
            const place_byte = try get_place_byte(i, place_first_byte, allocator);
            const value_byte = try get_value_byte(i, .Word, value, allocator, writer);

            const line = try std.fmt.allocPrint(allocator, "mov word {s}, {s}\n", .{ place_byte, value_byte });
            defer allocator.free(line);

            try writer.write_slice(line);

            i += 2;
        }

        if (has_one_byte) {
            const place_byte = try get_place_byte(i, place_first_byte, allocator);
            const value_byte = try get_value_byte(i, .Byte, value, allocator, writer);

            const line = try std.fmt.allocPrint(allocator, "mov byte {s}, {s}\n", .{ place_byte, value_byte });
            defer allocator.free(line);

            try writer.write_slice(line);

            i += 1; //Quite useless
        }
    }

    pub fn generate_stack_cleanup(size: Number, writer: *Writer(u8), allocator: std.mem.Allocator) !void {
        const output = try std.fmt.allocPrint(allocator, "add rsp, {d}\n", .{size.get(STACK_SIZE)});
        defer allocator.free(output);

        try writer.write_slice(output);
    }

    pub fn generate_ret(writer: *Writer(u8)) !void {
        try writer.write_slice("ret\n");
    }

    pub fn generate_call(fn_name: []const u8, writer: *Writer(u8)) !void {
        try writer.write_slice("call ");
        try writer.write_slice(fn_name);
        try writer.write_one('\n');
    }

    pub fn generate_cmp(base_value: LowLevelValue, compare_value: LowLevelValue, writer: *Writer(u8)) !void {
        try writer.write_slice("cmp ");
        //TODO generate the base and compare values
        _ = base_value;
        _ = compare_value;
        @panic("");
        // try writer.write_one('\n');
    }

    pub fn generator() Generator {
        return Generator{
            .vtable = .{
                .generate_section = Self.generate_section,
                .generate_global = Self.generate_global,
                .generate_label = Self.generate_label,
                .generate_jump_to_label = Self.generate_jump_to_label,
                .generate_stack_frame_start = Self.generate_stack_frame_start,
                .generate_stack_frame_end = Self.generate_stack_frame_end,
                .generate_stack_reservation = Self.generate_stack_reservation,
                .generate_stack_assignation = Self.generate_stack_assignation,
                .generate_stack_cleanup = Self.generate_stack_cleanup,
                .generate_ret = Self.generate_ret,
                .generate_call = Self.generate_call,
                .generate_cmp = Self.generate_cmp,
                .std_fns = StdFunctions,
            },
        };
    }
};
