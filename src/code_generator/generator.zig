const Command = @import("../low_level_output/low_level_output.zig").Command;
const std = @import("std");
const Writer = @import("../utils.zig").Writer;
const Number = @import("../tokenizer/number.zig").Number;
const StackAllocation = @import("../low_level_output/low_level_output.zig").StackAllocation;
const LowLevelValue = @import("../low_level_output/low_level_value.zig").LowLevelValue;

pub const Generator = struct {
    vtable: struct {
        generate_section: *const fn (section_name: []const u8, writer: *Writer(u8)) anyerror!void,
        generate_global: *const fn (fn_name: []const u8, writer: *Writer(u8)) anyerror!void,
        generate_label: *const fn (label: []const u8, writer: *Writer(u8)) anyerror!void,
        generate_jump_to_label: *const fn (label: []const u8, writer: *Writer(u8)) anyerror!void,
        generate_stack_frame_start: *const fn (writer: *Writer(u8)) anyerror!void,
        generate_stack_frame_end: *const fn (writer: *Writer(u8)) anyerror!void,

        generate_stack_reservation: *const fn (size: Number, writer: *Writer(u8), allocator: std.mem.Allocator) anyerror!void,

        generate_stack_assignation: *const fn (alloc: StackAllocation, value: LowLevelValue, writer: *Writer(u8), allocator: std.mem.Allocator, is_zeroed: bool) anyerror!void,
        generate_stack_cleanup: *const fn (size: Number, writer: *Writer(u8), allocator: std.mem.Allocator) anyerror!void,
        generate_ret: *const fn (writer: *Writer(u8)) anyerror!void,
        generate_call: *const fn (fn_name: []const u8, writer: *Writer(u8)) anyerror!void,
        generate_cmp: *const fn (base_value: LowLevelValue, compare_value: LowLevelValue, writer: *Writer(u8)) anyerror!void,
        std_fns: std.static_string_map.StaticStringMap([]const u8),
    },

    pub fn generate(self: Generator, input: []Command, allocator: std.mem.Allocator) ![]u8 {
        var output: [40_000]u8 = undefined;
        var writer = Writer(u8).init(&output);

        for (input) |command| {
            switch (command) {
                .Section => |section| try self.vtable.generate_section(section, &writer),
                .Global => |global_fn| try self.vtable.generate_global(global_fn, &writer),
                .Label => |label| try self.vtable.generate_label(label, &writer),
                .JmpToLabel => |label| try self.vtable.generate_jump_to_label(label, &writer),
                .StackFrameStart => try self.vtable.generate_stack_frame_start(&writer),
                .StackFrameEnd => try self.vtable.generate_stack_frame_end(&writer),
                .StackReservation => |size| try self.vtable.generate_stack_reservation(
                    size,
                    &writer,
                    allocator,
                ),
                .StackAssignation => |assignation| try self.vtable.generate_stack_assignation(
                    assignation.alloc,
                    assignation.value,
                    &writer,
                    allocator,
                    assignation.is_zeroed,
                ),
                .StackCleanup => |size| try self.vtable.generate_stack_cleanup(size, &writer, allocator),
                .Ret => try self.vtable.generate_ret(&writer),
                .Call => |fn_name| try self.vtable.generate_call(fn_name, &writer),
                .StdFnDeclaration => |fn_name| {
                    const function = self.vtable.std_fns.get(fn_name).?;
                    try writer.write_slice(function);
                },
                .Cmp => |cmp| try self.vtable.generate_cmp(cmp.base_value, cmp.compare_value, &writer),
                .ConditionalJump => |jmp| {
                    _ = jmp;
                    @panic("TODO");
                },
            }
        }

        const output_alloc = try allocator.alloc(u8, writer.written);
        @memcpy(output_alloc, output[0..writer.written]);

        return output_alloc;
    }
};
