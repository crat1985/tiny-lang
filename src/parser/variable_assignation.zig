const Value = @import("value.zig").Value;
const Reader = @import("../utils.zig").Reader;
const Token = @import("../tokenizer/token.zig").Token;
const std = @import("std");

pub const VariableAssignation = struct {
    var_name: []const u8,
    value: Value,
    is_zeroed: bool,
    start: usize,
    end: usize,

    const Self = @This();

    pub fn parse(reader: *Reader(Token), allocator: std.mem.Allocator) !Self {
        const first_token = reader.next().?;
        const start = first_token.start;

        const is_zeroed = switch (first_token.t.ident) {
            .Mov => false,
            .Movz => true,
            else => @panic("No"),
        };

        const var_name = reader.next().?.t.ident.Other;
        std.debug.assert(var_name.prefix == null);

        {
            const comma = reader.next().?.t.punct;
            std.debug.assert(comma == .Comma);
        }

        const value = try Value.parse(reader, allocator);

        const final_char = reader.next().?;

        std.debug.assert(final_char.t.punct == .SemiColon);

        return .{
            .var_name = var_name.value,
            .value = value,
            .is_zeroed = is_zeroed,
            .start = start,
            .end = final_char.end,
        };
    }
};
