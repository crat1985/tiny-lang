const std = @import("std");
const Value = @import("value.zig").Value;
const Reader = @import("../utils.zig").Reader;
const Token = @import("../tokenizer/token.zig").Token;
const FnChild = @import("fn_child.zig").FnChild;
const Punct = @import("../tokenizer/punct.zig").Punct;
const Block = @import("block.zig").Block;

pub const Operator = enum(u8) {
    Greater = '>',
    Less = '<',
    Equal = '=',
    NotEqual = '!', //TODO perhaps use several characters, not sure

    const Self = @This();

    pub fn parse(token: Punct) ?Self {
        inline for (std.meta.fields(Operator)) |field| {
            if (field.value == @intFromEnum(token)) {
                return @field(Operator, field.name);
            }
        }

        return null;
    }
};

pub const CmpChildren = struct { []Operator, Block };

pub const Cmp = struct {
    start: usize,
    end: usize,
    base_value: Value,
    compare_value: Value,
    children: std.ArrayList(CmpChildren),

    const Self = @This();

    pub fn parse(reader: *Reader(Token), allocator: std.mem.Allocator) anyerror!Self {
        const first_token = reader.next().?;
        const start = first_token.start;
        first_token.t.ident.Cmp; //Assert the first token is the ident `cmp`

        std.debug.assert(reader.next().?.t.punct == .OpenParenthesis);

        const base_value = try Value.parse(reader, allocator);

        std.debug.assert(reader.next().?.t.punct == .Comma);

        const compare_value = try Value.parse(reader, allocator);

        std.debug.assert(reader.next().?.t.punct == .ClosingParenthesis);

        std.debug.assert(reader.next().?.t.punct == .OpenBrace);

        var children = std.ArrayList(CmpChildren).init(allocator);

        const end = while (true) {
            const punct = reader.peek().?.t.punct;
            if (punct == .ClosingBrace) {
                const c = reader.next().?;
                break c.end;
            }

            const child = try Self.parse_cmp_child(reader, allocator);

            try children.append(.{ child.operators, child.child });
        };

        return .{
            .start = start,
            .end = end,
            .base_value = base_value,
            .compare_value = compare_value,
            .children = children,
        };
    }

    pub fn parse_cmp_child(reader: *Reader(Token), allocator: std.mem.Allocator) !struct {
        operators: []Operator,
        child: Block,
        end: usize,
    } {
        var operators = std.AutoHashMap(Operator, void).init(allocator);
        defer operators.deinit();

        //parse the operator(s)
        while (true) {
            const op = reader.peek().?;
            const punct = op.t.punct;
            switch (punct) {
                .OpenBrace => break,
                else => {},
            }

            _ = reader.next().?;

            const parsed_operator = Operator.parse(punct).?;
            const entry = try operators.getOrPut(parsed_operator);
            if (entry.found_existing) {
                std.debug.panic("Dupplicate operator {any}", .{parsed_operator});
            }
            entry.value_ptr.* = {};

            switch (reader.peek().?.t.punct) {
                .Comma => {
                    _ = reader.next().?;
                    if (reader.peek().?.t.punct == .OpenBrace) @panic("Expected operator, got open brace");
                    continue;
                },
                .OpenBrace => break,
                else => @panic("No"),
            }
        }

        //Parse the body
        const children = try Block.parse(reader, allocator, false);

        var operators_slice = try allocator.alloc(Operator, operators.count());
        var operators_iter = operators.keyIterator();

        for (operators_iter.items[0..operators.count()], 0..) |op, i| {
            operators_slice[i] = op;
        }

        return .{
            .operators = operators_slice,
            .child = children,
            .end = children.end,
        };
    }
};
