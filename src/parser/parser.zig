const std = @import("std");
const Token = @import("../tokenizer/token.zig").Token;
const utils = @import("../utils.zig");
const Reader = utils.Reader;
pub const FnDeclaration = @import("fn_declaration.zig").FnDeclaration;

pub const Parser = struct {
    fn_decls: std.StringHashMap(FnDeclaration),
    allocator: std.mem.Allocator,

    const Self = @This();

    pub fn parse(tokens: []Token, allocator: std.mem.Allocator) !Self {
        var fn_decls = std.StringHashMap(FnDeclaration).init(allocator);
        var reader = Reader(Token).init(tokens);

        while (reader.peek()) |token| {
            switch (token.t) {
                .ident => |ident| switch (ident) {
                    .Fn => {
                        const fn_decl = try FnDeclaration.parse(&reader, allocator);

                        const entry = try fn_decls.getOrPut(fn_decl.name);
                        if (entry.found_existing) {
                            std.debug.panic("Redeclaration of function {s}", .{fn_decl.name});
                        }
                        entry.value_ptr.* = fn_decl;
                    },

                    else => std.debug.panic("Expected function, got {any}", .{token}),
                },

                else => std.debug.panic("Expected function, got {any}", .{token}),
            }
        }

        const has_start_fn = fn_decls.contains("_start");

        if (!has_start_fn) {
            @panic("Missing _start function");
        }

        return .{ .fn_decls = fn_decls, .allocator = allocator };
    }

    pub fn set_fn_call_args_size_and_ret_size(self: *Self, std_fns: std.StringHashMap(FnDeclaration)) !void {
        var iterator = self.fn_decls.iterator();

        while (iterator.next()) |fn_decl| {
            const fn_children = fn_decl.value_ptr.body;
            for (fn_children) |*fn_child| {
                switch (fn_child.*) {
                    .fn_call => {},
                    else => continue,
                }

                const fn_call = &fn_child.fn_call;

                const got_fn = switch (fn_call.is_std) {
                    true => std_fns.get(fn_call.fn_name),
                    false => self.fn_decls.get(fn_call.fn_name),
                } orelse std.debug.panic("Undeclared function {s}", .{fn_call.fn_name});

                std.debug.assert(fn_call.args.len == got_fn.args.len);

                for (got_fn.args, 0..) |fn_arg, i| {
                    const fn_call_arg = &fn_call.args[i];
                    fn_call_arg.size = fn_arg.size;
                }

                fn_call.ret_size = got_fn.ret_size;
            }
        }
    }

    pub fn print_code(self: Self) void {
        var fn_decls_iterator = self.fn_decls.valueIterator();

        while (fn_decls_iterator.next()) |fn_decl| {
            utils.print_colored("fn ", .{}, .Red, .Default, true);
            std.debug.print("{s}(", .{fn_decl.name});
            for (fn_decl.args.items, 0..) |arg, i| {
                std.debug.print("{s}:", .{arg.name});

                utils.print_colored("{d}", .{arg.size}, .Blue, .Default, false);
                if (i + 1 != fn_decl.args.items.len) {
                    std.debug.print(", ", .{});
                }
            }
            std.debug.print(") {{\n", .{});

            for (fn_decl.body.items) |arg| {
                switch (arg) {
                    .fn_call => |fn_call| {
                        std.debug.print("{s}(", .{fn_call.fn_name});

                        //print the arguments
                        for (fn_call.args, 0..) |fn_call_arg, i| {
                            const value = std.mem.readVarInt(u64, fn_call_arg.value, .little);
                            utils.print_colored("{d}", .{value}, .Blue, .Default, false);
                            if (i + 1 == fn_call.args.len) {
                                std.debug.print(", ", .{});
                            }
                        }

                        std.debug.print(");\n", .{});
                    },
                    .variable_declaration => |var_decl| {
                        utils.print_colored("let ", .{}, .Red, .Default, true);

                        std.debug.print("{s} ", .{var_decl.name});

                        utils.print_colored("{d}", .{var_decl.size}, .Blue, .Default, false);

                        std.debug.print(";\n", .{});
                    },
                    else => @panic("Not implemented"),
                }
            }

            std.debug.print("}} \n\n", .{});
        }
    }

    //TODO
    // pub fn optimize(self: *Self) !void {}
};
