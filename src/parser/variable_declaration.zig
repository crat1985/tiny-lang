const Token = @import("../tokenizer/token.zig").Token;
const std = @import("std");
const Punct = @import("../tokenizer/punct.zig").Punct;
const Reader = @import("../utils.zig").Reader;
const Number = @import("../tokenizer/number.zig").Number;

/// Exemple :
/// ```
/// let variable_name: 2;
/// //Or
/// let variable_name: 2 = value;
/// ```
pub const VariableDeclaration = struct {
    name: []const u8,
    size: Number,
    start: usize,
    end: usize,

    const Self = @This();

    pub fn parse(tokens: *Reader(Token)) !Self {
        const start = tokens.peek().?.start;
        tokens.next().?.t.ident.Let;

        const name = tokens.next().?.t.ident.Other;
        std.debug.assert(name.prefix == null);

        std.debug.assert(tokens.next().?.t.punct == .Colon);

        const size = switch (tokens.next().?.t) {
            .number_literal => |nb| Number{ .literal = nb },
            .ident => |ident| blk: {
                ident.Ref;
                break :blk Number{ .ref_count = 1 };
            },
            else => |token| std.debug.panic("Expected number, got {any}", .{token}),
        };

        const semi_colon = tokens.next().?;

        std.debug.assert(semi_colon.t.punct == Punct.SemiColon);

        return .{
            .name = name.value,
            .size = size,
            .start = start,
            .end = semi_colon.end,
        };
    }
};
