const std = @import("std");
const FnChild = @import("fn_child.zig").FnChild;
const Token = @import("../tokenizer/token.zig").Token;
const VariableDeclaration = @import("variable_declaration.zig").VariableDeclaration;
const FnCall = @import("fn_call.zig").FnCall;
const Reader = @import("../utils.zig").Reader;
const Number = @import("../tokenizer/number.zig").Number;
const FnCallArg = @import("fn_call.zig").FnCallArg;

pub const FnDeclaration =
    struct {
    name: []const u8,
    args: []FnArg,
    body: []FnChild,
    ret_size: Number,
    start: usize,
    end: usize,

    const Self = @This();

    pub fn parse(tokens: *Reader(Token), allocator: std.mem.Allocator) !Self {
        const fn_decl_start = tokens.peek().?.start;
        //The `fn` keyword
        tokens.next().?.t.ident.Fn;

        //The fn name
        const name = tokens.next().?.t.ident.Other;
        std.debug.assert(name.prefix == null);

        //The open parenthesis
        switch (tokens.next().?.t.punct) {
            .OpenParenthesis => {},
            else => |punct| std.debug.panic("Expected open parenthesis, got {any}", .{punct}),
        }

        //The args
        var args = std.ArrayList(FnArg).init(allocator);
        while (true) {
            switch (tokens.peek().?.t) {
                .ident => {
                    const arg = try FnArg.parse(tokens);
                    try args.append(arg);
                },

                else => break,
            }

            switch (tokens.peek().?.t) {
                .punct => |punct| switch (punct) {
                    .Comma => {
                        _ = tokens.next().?;
                    },
                    else => break,
                },
                else => break,
            }
        }

        //The closing parenthesis
        switch (tokens.next().?.t.punct) {
            .ClosingParenthesis => {},
            else => |punct| std.debug.panic("Expected close parenthesis, got {any}", .{punct}),
        }

        //The optional ret size
        const ret_size = switch (tokens.peek().?.t) {
            .number_literal => |nb| blk: {
                _ = tokens.next().?;
                break :blk Number{ .literal = nb };
            },
            .ident => |nb| blk: {
                _ = tokens.next().?;
                nb.Ref;
                break :blk Number{ .ref_count = 1 };
            },
            else => Number{ .literal = 0 },
        };

        //The open brace
        switch (tokens.next().?.t.punct) {
            .OpenBrace => {},
            else => |punct| std.debug.panic("Expected open brace, got {any}", .{punct}),
        }

        //The body
        var fn_children = try FnChild.parse_fn_children(tokens, allocator);

        var exit_args = std.ArrayList(FnCallArg).init(allocator);
        try exit_args.append(.{ .value = .{ .literal = Number{} }, .size = Number{ .ref_count = 1 }, .start = 0, .end = 0 });

        //TODO not sure if this should be automatically added if not present
        if (std.mem.eql(u8, name.value, "_start")) {
            fn_children.value = try allocator.realloc(fn_children.value, fn_children.value.len + 1);
            fn_children.value[fn_children.value.len - 1] = FnChild{ .fn_call = .{
                .fn_name = "exit",
                .is_std = true,
                .args = try exit_args.toOwnedSlice(),
                .ret_size = Number{},
                .start = 0,
                .end = 0,
            } };
        }

        return .{
            .name = name.value,
            .args = try args.toOwnedSlice(),
            .body = fn_children.value,
            .start = fn_decl_start,
            .end = fn_children.end,
            .ret_size = ret_size,
        };
    }
};

pub const FnArg = struct {
    name: []const u8,
    size: Number,
    start: usize,
    end: usize,

    pub fn parse(tokens: *Reader(Token)) !FnArg {
        const fn_arg_start = tokens.peek().?.start;
        const name = tokens.next().?.t.ident.Other;

        switch (tokens.next().?.t.punct) {
            .Colon => {},
            else => return error.MissingArgComma,
        }

        const size_token = tokens.next().?;

        const size = switch (size_token.t) {
            .number_literal => |nb| Number{ .literal = nb },
            .ident => |ident| blk: {
                ident.Ref;
                break :blk Number{ .ref_count = 1 };
            },
            else => |token| std.debug.panic("Expected number literal or `ref`, got {any}", .{token}),
        };

        return FnArg{
            .name = name.value,
            .size = size,
            .start = fn_arg_start,
            .end = size_token.end,
        };
    }
};
