const Reader = @import("../utils.zig").Reader;
const Token = @import("../tokenizer/token.zig").Token;
const std = @import("std");
const Punct = @import("../tokenizer/punct.zig").Punct;

pub const LoopBreak = struct {
    start: usize,
    end: usize,
    break_id: u64,

    pub fn parse(reader: *Reader(Token)) LoopBreak {
        const start = reader.peek().?.start;
        reader.next().?.t.ident.Break;

        var break_loop_id: u64 = undefined;

        const semi_colon = blk: {
            const perhaps_semi_colon = reader.next().?;
            switch (perhaps_semi_colon.t) {
                .punct => {
                    break_loop_id = 0;
                    break :blk perhaps_semi_colon;
                },
                .number_literal => |nb| {
                    break_loop_id = nb;
                    break :blk reader.next().?;
                },

                else => std.debug.panic("Expected `;` or a number literal, got {any}", .{perhaps_semi_colon}),
            }
        };

        const end = semi_colon.end;
        std.debug.assert(semi_colon.t.punct == Punct.SemiColon);

        return .{
            .start = start,
            .end = end,
            .break_id = break_loop_id,
        };
    }
};
