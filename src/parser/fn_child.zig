const FnCall = @import("fn_call.zig").FnCall;
const VariableDeclaration = @import("variable_declaration.zig").VariableDeclaration;
const VariableAssignation = @import("variable_assignation.zig").VariableAssignation;
const Reader = @import("../utils.zig").Reader;
const Token = @import("../tokenizer/token.zig").Token;
const std = @import("std");
const Punct = @import("../tokenizer/punct.zig").Punct;
const FnReturn = @import("fn_return.zig").FnReturn;
const Block = @import("block.zig").Block;
const LoopBreak = @import("loop_break.zig").LoopBreak;
const LoopContinue = @import("loop_continue.zig").LoopContinue;
const Cmp = @import("cmp.zig").Cmp;

pub const FnChild = union(enum) {
    fn_call: FnCall,
    variable_declaration: VariableDeclaration,
    ret: FnReturn,
    block: Block,
    @"break": LoopBreak,
    @"continue": LoopContinue,
    variable_assignation: VariableAssignation,
    cmp: Cmp,

    const Self = @This();

    pub fn parse(reader: *Reader(Token), allocator: std.mem.Allocator) !Self {
        return switch (reader.peek().?.t) {
            .ident => |ident| switch (ident) {
                .Let => Self{ .variable_declaration = try VariableDeclaration.parse(reader) },
                .Other => switch (reader.n_peek(1).?.t.punct) {
                    .OpenParenthesis => blk: {
                        const fn_call =
                            try FnCall.parse(reader, allocator);

                        std.debug.assert(reader.next().?.t.punct == .SemiColon);

                        break :blk Self{ .fn_call = fn_call };
                    },
                    else => @panic("No"),
                },
                .Return => Self{ .ret = try FnReturn.parse(reader, allocator) },
                .Loop => blk: {
                    _ = reader.next().?;
                    break :blk Self{ .block = try Block.parse(reader, allocator, true) };
                },
                .Mov, .Movz => Self{ .variable_assignation = try VariableAssignation.parse(reader, allocator) },
                .Break => Self{ .@"break" = LoopBreak.parse(reader) },
                .Continue => Self{ .@"continue" = LoopContinue.parse(reader) },
                .Cmp => Self{ .cmp = try Cmp.parse(reader, allocator) },
                else => std.debug.panic("Expected variable declaration or function call, got {any}", .{ident}),
            },
            .punct => |punct| blk: {
                if (punct != .OpenBrace) std.debug.panic("Expected open brace, got {any}", .{punct});
                break :blk Self{ .block = try Block.parse(reader, allocator, false) };
            },
            else => |token| std.debug.panic("Expected a fn child, got {any}", .{token}),
        };
    }

    pub fn parse_fn_children(reader: *Reader(Token), allocator: std.mem.Allocator) !struct { value: []Self, end: usize } {
        var children = std.ArrayList(Self).init(allocator);

        const end = while (true) {
            const token = reader.peek().?;

            switch (token.t) {
                .punct => |punct| if (punct ==
                    .ClosingBrace)
                {
                    _ = reader.next().?;
                    break token.end;
                },

                else => {},
            }

            try children.append(try FnChild.parse(reader, allocator));
        };

        return .{ .value = try children.toOwnedSlice(), .end = end };
    }
};
