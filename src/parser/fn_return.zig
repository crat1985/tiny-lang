const Reader = @import("../utils.zig").Reader;
const Token = @import("../tokenizer/token.zig").Token;
const std = @import("std");
const Punct = @import("../tokenizer/punct.zig").Punct;
const Number = @import("../tokenizer/number.zig").Number;
const Value = @import("value.zig").Value;

pub const FnReturn = struct {
    value: ?Value,
    start: usize,
    end: usize,

    pub fn parse(reader: *Reader(Token), allocator: std.mem.Allocator) !FnReturn {
        const start = reader.peek().?.start;
        reader.next().?.t.ident.Return;

        const value: ?Value = switch (reader.peek().?.t) {
            .punct => null,
            else => try Value.parse(reader, allocator),
        };

        const end = reader.peek().?.end;
        std.debug.assert(reader.next().?.t.punct == Punct.SemiColon);

        return .{
            .value = value,
            .start = start,
            .end = end,
        };
    }
};
