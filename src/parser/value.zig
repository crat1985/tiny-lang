const FnCall = @import("fn_call.zig").FnCall;
const Number = @import("../tokenizer/number.zig").Number;
const Reader = @import("../utils.zig").Reader;
const Token = @import("../tokenizer/token.zig").Token;
const std = @import("std");

pub const Value = union(enum) {
    fn_call: FnCall,
    literal: Number,
    variable: []const u8,

    const Self = @This();

    pub fn parse(reader: *Reader(Token), allocator: std.mem.Allocator) anyerror!Self {
        const first_token = reader.peek().?;
        return switch (first_token.t) {
            .ident => |ident| switch (ident) {
                .Ref => {
                    _ = reader.next().?;
                    return Self{ .literal = Number{ .ref_count = 1 } };
                },
                .Other => |name| switch (reader.n_peek(1).?.t.punct) {
                    .OpenParenthesis => Self{ .fn_call = try FnCall.parse(reader, allocator) },
                    else => blk: {
                        _ = reader.next().?;
                        std.debug.assert(name.prefix == null);
                        break :blk Self{ .variable = name.value };
                    },
                },
                else => std.debug.panic("Expected value, got {any}", .{first_token}),
            },
            .number_literal => |nb| {
                _ = reader.next().?;
                return Self{ .literal = Number{ .literal = nb } };
            },
            else => std.debug.panic("Expected value (ident or number literal), got {any}", .{first_token}),
        };
    }
};
