const Token = @import("../tokenizer/token.zig").Token;
const std = @import("std");
const Reader = @import("../utils.zig").Reader;
const Punct = @import("../tokenizer/punct.zig").Punct;
const Number = @import("../tokenizer/number.zig").Number;
const Value = @import("value.zig").Value;

pub const FnCallArg = struct {
    value: Value,
    size: Number,
    start: usize,
    end: usize,
};

pub const FnCall =
    struct {
    fn_name: []const u8,
    is_std: bool,
    args: []FnCallArg,
    ret_size: Number,
    start: usize,
    end: usize,

    pub fn parse(tokens: *Reader(Token), allocator: std.mem.Allocator) !FnCall {
        const fn_call_start: usize = tokens.peek().?.start;
        const fn_name = tokens.next().?.t.ident.Other;
        const is_std: bool = if (fn_name.prefix == .At) true else false;

        switch (tokens.next().?.t.punct) {
            .OpenParenthesis => {},
            else => |punct| std.debug.panic("Expected open parenthesis, got {any}, at fn call at {d}", .{ punct, fn_call_start }),
        }

        var args = std.ArrayList(FnCallArg).init(allocator);

        while (true) {
            const start = tokens.peek().?.start;

            const arg_value = switch (tokens.peek().?.t) {
                .punct => |punct| {
                    switch (punct) {
                        .ClosingParenthesis => {
                            _ = tokens.next().?;
                            break;
                        },
                        else => std.debug.panic("Expected closing parenthesis, got {any}, at fn call at {d}", .{ punct, fn_call_start }),
                    }
                },
                else => try Value.parse(tokens, allocator),
            };

            //TODO improve
            const end = tokens.peek().?.start;

            try args.append(FnCallArg{
                .value = arg_value,
                .size = undefined,
                .start = start,
                .end = end,
            });

            // _ = tokens.next().?;

            const punct = tokens.next().?.t.punct;

            switch (punct) {
                .Comma => {},
                .ClosingParenthesis => break,
                else => |punct_inner| std.debug.panic("Expected `,`, got {any}, at fn call at {d}", .{ punct_inner, fn_call_start }),
            }
        }

        const semi_colon = tokens.peek().?;

        switch (semi_colon.t.punct) {
            .SemiColon => {},
            else => |punct| std.debug.panic("Expected `;`, got {any}", .{punct}),
        }

        return .{
            .is_std = is_std,
            .fn_name = fn_name.value,
            .args = try args.toOwnedSlice(),
            .start = fn_call_start,
            .end = semi_colon.end,
            .ret_size = undefined,
        };
    }
};
