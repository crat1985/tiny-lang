const Reader = @import("../utils.zig").Reader;
const Token = @import("../tokenizer/token.zig").Token;
const std = @import("std");
const Punct = @import("../tokenizer/punct.zig").Punct;

pub const LoopContinue = struct {
    start: usize,
    end: usize,
    continue_id: u64,

    pub fn parse(reader: *Reader(Token)) LoopContinue {
        const start = reader.peek().?.start;
        reader.next().?.t.ident.Continue;

        var continue_id: u64 = undefined;

        const semi_colon = blk: {
            const perhaps_semi_colon = reader.next().?;
            switch (perhaps_semi_colon.t) {
                .punct => {
                    continue_id = 0;
                    break :blk perhaps_semi_colon;
                },
                .number_literal => |nb| {
                    continue_id = nb;
                    break :blk reader.next().?;
                },
                else => std.debug.panic("Expected `;` or a number literal, got {any}", .{perhaps_semi_colon}),
            }
        };

        const end = semi_colon.end;
        std.debug.assert(semi_colon.t.punct == Punct.SemiColon);

        return .{
            .start = start,
            .end = end,
            .continue_id = continue_id,
        };
    }
};
