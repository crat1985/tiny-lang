const VariableDeclaration = @import("variable_declaration.zig").VariableDeclaration;
const FnCall = @import("fn_call.zig").FnCall;
const FnReturn = @import("fn_return.zig").FnReturn;
const Reader = @import("../utils.zig").Reader;
const Token = @import("../tokenizer/token.zig").Token;
const Punct = @import("../tokenizer/punct.zig").Punct;
const std = @import("std");
const LoopBreak = @import("loop_break.zig").LoopBreak;
const LoopContinue = @import("loop_continue.zig").LoopContinue;
const FnChild = @import("fn_child.zig").FnChild;

pub const Block = struct {
    start: usize,
    end: usize,
    children: []FnChild,
    is_loop: bool,

    pub fn parse(reader: *Reader(Token), allocator: std.mem.Allocator, is_loop: bool) anyerror!Block {
        const start = blk: {
            const first_token = reader.next().?;
            const start = first_token.start;

            std.debug.assert(first_token.t.punct == .OpenBrace);

            break :blk start;
        };

        //The body
        const value = try FnChild.parse_fn_children(reader, allocator);

        return .{
            .start = start,
            .end = value.end,
            .children = value.value,
            .is_loop = is_loop,
        };
    }
};
