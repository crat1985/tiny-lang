# Tiny lang

## TODO
- [x] Function declaration with arguments and a return size
- [x] Variable declaration inside functions
- [x] Function calls inside functions
- [x] Keyword `ref`, which represent an unsigned integer, the size of a ptr for the target arch (8 for 64bit, 4 for 32bit...)
- [x] Std calls (`@fn_name(args...)`)
- [x] Return with an optional value
- [x] Variable assignation (`mov variable_name, value`)
- [ ] Loops with the ability to specify the loop when using `break` and `continue` (0 for the closer loop, 1 for the first outer one...)
  - [x] Basic
  - [ ] Clean the stack at each iteration
- [ ] Cmp (like Assembly's `cmp` instruction)
- [ ] References (get the addr with `&value` and dereference it using `deref <size> ptr`)
- [ ] Blocks
- [ ] Declare characters as a byte
- [ ] Declare constant strings (allocated in the .data section)
